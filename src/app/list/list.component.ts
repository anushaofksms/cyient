import { Component, OnInit } from '@angular/core';
import { ListService } from './../list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  
  list:any;
  dataList:any;
  addClickButton:boolean;
  showAddForm:boolean;
  addButton:boolean;
  elementIndex:number;
  constructor(private listService:ListService) { 
    this.addClickButton = false;
  }

  ngOnInit() {
    this.getData()
    this.showAddForm = false;
  }

  getData()
  {
    return this.listService.getData().subscribe(response =>
      {
        this.list = response
      })
  }

  addClick()
  {
    this.showAddForm = true;
  }

  dataClick(data,i)
  {
    this.elementIndex = i;
    this.dataList = data;
    this.addClickButton = true;
  }

  getEditData(respone)
  {
    for(let index=0;index<this.list.length;index++)
    {
      if(this.list[index].id == respone.id)
      {
        this.list[index].name = respone.name;
        this.list[index].type = respone.type;
      }
    }
    this.addClickButton = false;
  }

  getAddedData(dataAdded)
  {
     this.list.push(dataAdded);
     this.addClickButton = false;
  }

  getDeletedId(did)
  {
     console.log(did);
    this.list.splice(did,1);
    this.addClickButton = false;
  }
}

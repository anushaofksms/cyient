import { Component, OnInit , Input, Output ,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  

  // @Input() addClicked : boolean;
  @Input() showAddForm:any;
  @Input() showData:any;
  @Input() indexElement:any;
  @Output() editedData = new EventEmitter<any>();
  @Output() addedData = new EventEmitter<any>();
  editForm:boolean;
  addForm:boolean;
  empId:any;
  empName:string;
  empType:string;
  empOcc:string;

  @Output() deletedData = new EventEmitter<any>();
  empIndex:any;

  fetchData={
    "id":"",
    "name":"",
    "type":"",
    "occ":""
  }
  constructor() { 
    this.editForm = false;
    this.addForm = true;
  }

  ngOnInit() { 
  }

  ngOnChanges(changes) {
    if(changes.showData)
    {
        this.empId = changes.showData.currentValue.id;
        this.empName = changes.showData.currentValue.name;
        this.empType = changes.showData.currentValue.type;
        this.empOcc = changes.showData.currentValue.occ;
    }
    
    if(changes.showAddForm)
    {
      this.showAddForm = changes.showAddForm.currentValue;
    }
  }
  
  editButtonClick()
  {
    this.addForm = false;
    this.editForm = true;
    this.fetchData.id = this.empId;
    this.fetchData.name = this.empName;
    this.fetchData.type = this.empType;
    this.fetchData.occ = this.empOcc;
  }
  
  addData(dataAdd)
  {
    this.addedData.emit(dataAdd);
  }

  editData(dataedit)
  {
    this.editedData.emit(dataedit);
  }
  
  deleteButtonClick()
  {
    this.deletedData.emit(this.empIndex)
  }
}
